package commands

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
	"strings"

	"git.roshless.me/roshless/mumbledj/models"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/vincent-petithory/dataurl"
	"layeh.com/gumble/gumble"
)

// AddLocalStorage is a command that adds an audio track to a local storage.
type AddLocalStorage struct{}

// Aliases returns the current aliases for the command.
func (c *AddLocalStorage) Aliases() []string {
	return viper.GetStringSlice("commands.addls.aliases")
}

// Description returns the description for the command.
func (c *AddLocalStorage) Description() string {
	return viper.GetString("commands.addls.description")
}

// IsAdminCommand returns true if the command is only for admin use, and
// returns false otherwise.
func (c *AddLocalStorage) IsAdminCommand() bool {
	return viper.GetBool("commands.addls.is_admin")
}

// Execute executes the command with the given user and arguments.
// Return value descriptions:
//
//	string: A message to be returned to the user upon successful execution.
//	bool:   Whether the message should be private or not. true = private,
//	        false = public (sent to whole channel).
//	error:  An error message to be returned upon unsuccessful execution.
//	        If no error has occurred, pass nil instead.
//
// Example return statement:
//
//	return "This is a private message!", true, nil
func (c *AddLocalStorage) Execute(user *gumble.User, args ...string) (string, bool, error) {
	var (
		resp       *http.Response
		err        error
		ytResponse models.YoutubeResponse
		newTrack   models.LocalStorageInfo
		cmd        *exec.Cmd
	)

	if !viper.GetBool("localstorage.enabled") {
		return "", true, errors.New(viper.GetString("common_messages.disabled_error"))
	}

	if len(args) != 2 {
		return "", true, errors.New(viper.GetString("commands.addls.messages.syntax_error"))
	}

	newTrack.Url = args[0]
	tag := args[1]

	idUnformatted := strings.Split(newTrack.Url, "watch?v=")
	if len(idUnformatted) != 2 {
		return "", true, errors.New(viper.GetString("commands.addls.messages.syntax_error"))
	}
	id := idUnformatted[1]

	videoURL := "https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails&id=%s&key=%s"
	// Get info about video from youtube
	resp, err = http.Get(fmt.Sprintf(videoURL, id, viper.GetString("api_keys.youtube")))
	if err != nil {
		return "", true, errors.New(viper.GetString("commands.addls.messages.api_error"))
	}
	defer resp.Body.Close()

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", true, err
	}

	err = json.Unmarshal(respBytes, &ytResponse)
	if err != nil {
		return "", true, errors.New(viper.GetString("commands.addls.messages.parsing_error"))
	}

	item := ytResponse.Items[0]
	newTrack.Title = item.Snippet.Title
	thumbnailUrl := item.Snippet.Thumbnails.High.URL
	newTrack.Artist = item.Snippet.ChannelTitle
	newTrack.DurationString, _ = strings.CutPrefix(strings.ToLower(item.ContentDetails.Duration), "pt")

	// Download file to local storage folder
	filePath := viper.GetString("localstorage.directory") + "/" + tag + ".track"
	cmd = exec.Command("youtube-dl", "--verbose", "--no-mtime", "--output", filePath, "--format", "bestaudio", newTrack.Url)
	output, err := cmd.CombinedOutput()
	if err != nil {
		args := ""
		for s := range cmd.Args {
			args += cmd.Args[s] + " "
		}
		logrus.Warnf("%s\n%s\nyoutube-dl: %s", args, string(output), err.Error())
		return "", true, errors.New("track download failed")
	}

	// Mumble no longer displays links to images so we have to convert it into data uri
	resp, err = http.Get(thumbnailUrl)
	if err != nil {
		return "", true, errors.New("couldn't get thumbnail image")
	}
	defer resp.Body.Close()

	buf := bytes.NewBuffer(make([]byte, 0, resp.ContentLength))
	_, err = buf.ReadFrom(resp.Body)
	if err != nil {
		return "", true, err
	}
	img := buf.Bytes()

	newTrack.Thumbnail = dataurl.EncodeBytes(img)

	newTrackBytes, err := json.Marshal(newTrack)
	if err != nil {
		return "", true, err
	}

	// Create json file with info
	jsonFilePath := viper.GetString("localstorage.directory") + "/" + tag + ".json"
	err = os.WriteFile(jsonFilePath, newTrackBytes, 0644)
	if err != nil {
		return "", true, err
	}

	retString := fmt.Sprintf(viper.GetString("commands.addls.messages.track_added"), tag, newTrack.Url)
	return retString, true, nil
}

/*
 * MumbleDJ
 * By Matthieu Grieger
 * services/localstorage.go
 * Copyright (c) 2018 Roshless (MIT License)
 */

package services

import (
	"encoding/json"
	"errors"
	"os"
	"regexp"
	"strings"
	"time"

	"git.roshless.me/roshless/mumbledj/bot"
	"git.roshless.me/roshless/mumbledj/interfaces"
	"git.roshless.me/roshless/mumbledj/models"
	"github.com/spf13/viper"
	"layeh.com/gumble/gumble"
)

// LocalStorage is simple local file player.
type LocalStorage struct {
	*GenericService
}

// NewLocalStorageService returns an initialized LocalStorage service object
func NewLocalStorageService() *LocalStorage {
	return &LocalStorage{
		&GenericService{
			ReadableName: "LocalStorage",
			Format:       "",
			TrackRegex: []*regexp.Regexp{
				regexp.MustCompile(`.+\.ls`),
			},
			PlaylistRegex: nil,
		},
	}
}

// CheckAPIKey performs a test API call with the API key
// provided in the configuration file to determine if the
// service should be enabled.
func (ls *LocalStorage) CheckAPIKey() error {
	// We check for
	if !viper.GetBool("localstorage.enabled") {
		return errors.New("LocalStorage disabled")
	}
	return nil
}

// GetTracks uses the passed tag to find and return
// tracks associated with the tag (file name without extension).
// An error is returned if any error occurs during the API call.
func (ls *LocalStorage) GetTracks(tag string, submitter *gumble.User) ([]interfaces.Track, error) {
	var (
		id        string
		err       error
		directory string
		filePath  string
		jsonPath  string
		v         models.LocalStorageInfo
		tracks    []interfaces.Track
	)

	fileExtension := ".track"
	tagSplit := strings.Split(tag, ".")
	id = tagSplit[0]

	directory = viper.GetString("localstorage.directory")
	if !strings.HasSuffix(directory, "/") {
		directory += "/"
	}

	if _, err = os.ReadDir(viper.GetString("localstorage.directory")); err != nil {
		return nil, errors.New(viper.GetString("localstorage.common_messages.directory_error"))
	}

	filePath = directory + id + fileExtension
	if _, err = os.Stat(filePath); os.IsNotExist(err) {
		return nil, err
	}

	jsonPath = directory + id + ".json"
	jsonFile, err := os.ReadFile(jsonPath)
	if err != nil {
		return nil, err
	}

	err = json.Unmarshal(jsonFile, &v)
	if err != nil {
		return nil, err
	}

	duration, err := time.ParseDuration(v.DurationString)
	if err != nil {
		return nil, err
	}

	offset, err := time.ParseDuration("0s")
	if err != nil {
		return nil, err
	}

	track := bot.Track{
		ID:             id,
		URL:            v.Url,
		Title:          v.Title,
		Author:         v.Artist,
		Submitter:      submitter.Name,
		Service:        ls.ReadableName,
		Filename:       id + fileExtension,
		ThumbnailURL:   v.Thumbnail,
		Duration:       duration,
		PlaybackOffset: offset,
		Playlist:       nil,
	}

	tracks = append(tracks, track)
	return tracks, nil
}

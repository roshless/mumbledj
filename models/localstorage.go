package models

type LocalStorageInfo struct {
	Url            string `json:"url"`
	Title          string `json:"title"`
	Thumbnail      string `json:"thumbnail"`
	Artist         string `json:"artist"`
	DurationString string `json:"duration"`
}

# Patches

You can send patches to [the mailing list](https://lists.roshless.me/~roshless/mumbledj).

If you don't know how, read this great [interactive tutorial](https://git-send-email.io/) by SourceHut community. It's really not that hard, if you can write code you can do it :)

I general I recommend to write the idea to mailing list first, so you don't waste your time in case I might have issues with the feature. Keep in mind I'm more for reducing the scope of this bot, not increasing it.
For bugfixes, just send the patch.

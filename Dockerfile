FROM golang:1.21-alpine AS builder

RUN apk add --no-cache ca-certificates ffmpeg make build-base opus-dev aria2 wget

WORKDIR /usr/src/app

RUN wget https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -O youtube-dl && chmod a+x youtube-dl

COPY . .
RUN go build -o mumbledj -v .

FROM alpine:3.18

RUN apk add --no-cache ca-certificates python3 ffmpeg aria2 openssl

RUN adduser -D app
COPY --from=builder --chown=app /usr/src/app/mumbledj /usr/local/bin/mumbledj
COPY --from=builder --chown=app /usr/src/app/youtube-dl /bin/youtube-dl

ENTRYPOINT ["/usr/local/bin/mumbledj"]

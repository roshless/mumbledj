package main

import (
	"os"
	"strings"

	"git.roshless.me/roshless/mumbledj/bot"
	"git.roshless.me/roshless/mumbledj/commands"
	"git.roshless.me/roshless/mumbledj/services"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"github.com/urfave/cli"
)

// DJ is a global variable that holds various details about the bot's state.
var DJ = bot.NewMumbleDJ()

func init() {
	DJ.Commands = commands.Commands
	DJ.AvailableServices = services.Services

	// Injection into sub-packages.
	commands.DJ = DJ
	services.DJ = DJ
	bot.DJ = DJ

	DJ.Version = "v4.0.0"

	logrus.SetLevel(logrus.WarnLevel)
}

func main() {
	app := cli.NewApp()
	app.Name = "MumbleDJ"
	app.Usage = "A Mumble bot that plays audio from various media sources."
	app.Version = DJ.Version
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config, c",
			Value: os.ExpandEnv("$HOME/.config/mumbledj/config.yaml"),
			Usage: "location of MumbleDJ configuration file",
			EnvVar: "MUMBLEDJ_CONFIG_PATH",
		},
		cli.BoolFlag{
			Name:  "debug, d",
			Usage: "if present, all debug messages will be shown",
		},
	}

	app.Action = func(c *cli.Context) error {
		if c.Bool("debug") {
			logrus.SetLevel(logrus.InfoLevel)
		}

		for _, configValue := range viper.AllKeys() {
			if c.GlobalIsSet(configValue) {
				if strings.Contains(c.String(configValue), ",") {
					viper.Set(configValue, strings.Split(c.String(configValue), ","))
				} else {
					viper.Set(configValue, c.String(configValue))
				}
			}
		}

		viper.SetConfigFile(c.String("config"))
		if err := viper.ReadInConfig(); err != nil {
			logrus.WithFields(logrus.Fields{
				"file":  c.String("config"),
				"error": err.Error(),
			}).Fatalln("An error occurred while reading the configuration file.")
		}
		if duplicateErr := bot.CheckForDuplicateAliases(); duplicateErr != nil {
			logrus.WithFields(logrus.Fields{
				"issue": duplicateErr.Error(),
			}).Fatalln("An issue was discoverd in your configuration.")
		}
		viper.WatchConfig()

		if err := DJ.Connect(); err != nil {
			logrus.WithFields(logrus.Fields{
				"error": err.Error(),
			}).Fatalln("An error occurred while connecting to the server.")
		}

		if viper.GetString("defaults.channel") != "" {
			defaultChannel := strings.Split(viper.GetString("defaults.channel"), "/")
			DJ.Client.Do(func() {
				DJ.Client.Self.Move(DJ.Client.Channels.Find(defaultChannel...))
			})
		}

		DJ.Client.Do(func() {
			DJ.Client.Self.SetComment(viper.GetString("defaults.comment"))
		})
		<-DJ.KeepAlive

		return nil
	}

	app.Run(os.Args)
}

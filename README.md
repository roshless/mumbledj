<h1 align="center">MumbleDJ</h1>
<p align="center"><b>A Mumble bot that plays audio fetched from various media websites.</b></p>

Non-dead fork of [matthieugrieger's mumbledj](https://github.com/matthieugrieger/mumbledj)

## Features
* Plays audio from many media websites, including YouTube, SoundCloud, and Mixcloud.
* Can also download from YouTube and play audio locally (disabled by default).
* Supports playlists and individual videos/tracks.
* Displays metadata in the text chat whenever a new track starts playing.
* A large array of [commands](#commands) that perform a wide variety of functions.
* Supports local storage playback with custom tags, based on youtube downloads.
* Built-in vote-skipping.
* Built-in caching system (disabled by default).
* Built-in play/pause/volume control.

### Requirements
* [`yt-dlp`](https://github.com/yt-dlp/yt-dlp)
* [`ffmpeg`](https://ffmpeg.org)
* [`aria2`](https://aria2.github.io/) if you plan on using services that throttle download speeds (like Mixcloud)

uMurmur servers *are not supported* due to bandwith limits.

#### YouTube API Key
A YouTube API key must be present in your configuration file in order to use the YouTube service within the bot. Below is a guide for retrieving an API key:

**1)** Navigate to the [Google Developers Console](https://console.developers.google.com) and sign in with your Google account, or create one if you haven't already.

**2)** Click the "Create Project" button and give your project a name. It doesn't matter what you set your project name to. Once you have a name click the "Create" button. You should be redirected to your new project once it's ready.

**3)** Click on "APIs & auth" on the sidebar, and then click APIs. Under the "YouTube APIs" header, click "YouTube Data API". Click on the "Enable API" button.

**4)** Click on the "Credentials" option underneath "APIs & auth" on the sidebar. Underneath "Public API access" click on "Create New Key". Choose the "Server key" option.

**5)** Add the IP address of the machine MumbleDJ will run on in the box that appears (this is optional, but improves security). Click "Create".

**6)** You should now see that an API key has been generated. Copy/paste this API key into the configuration file located at `$HOME/.config/mumbledj/mumbledj.yaml`.

#### SoundCloud API Key
A SoundCloud client ID must be present in your configuration file in order to use the SoundCloud service within the bot. Below is a guide for retrieving a client ID:

**1)** Login/sign up for a SoundCloud account on https://soundcloud.com.

**2)** Create a new app: https://soundcloud.com/you/apps/new.

**3)** You should now see that a client ID has been generated. Copy/paste this ID (NOT the client secret) into the configuration file located at `$HOME/.config/mumbledj/mumbledj.yaml`.


### Installation- containers

The only supported way to run this bot is via Kubernetes or Docker/Podman.

You can find working K8S config inside [This repo](https://git.roshless.me/infra/kubernetes-deploy)

#### Buidling your own image

First you need to clone the MumbleDJ repository to your machine:
```
git clone https://git.roshless.me/roshless/mumbledj
```

Then build the image:
```
docker build -t mumbledj .
```

#### Using prebuild images

You can also use images automaticcly build from this repository:
```
registry.roshless.me/roshless/mumbledj
```

Supported tags are:
* latest- development version
* datetime based e.g. 202310052015- development version that will not change (until it is deleted)
* stable- last stable release
* versioned stable- e.g. v4.0.0

